package main.org.myblog.example.controller;


import main.org.myblog.example.model.entity.Register;
import main.org.myblog.example.model.service.RegisterService;

import javax.ws.rs.*;

@Path("/register")
public class RegisterController {
    @Path("/save")
    @GET
    @Produces("application/json")
    public String save(@QueryParam("name") String name, @QueryParam("email") String email, @QueryParam("password") String password) {
        RegisterService.getInstance().save(new Register(name, email, password));
        return RegisterService.getInstance().findAll();
    }

    @Path("/update")
    @GET
    @Produces("application/json")
    public String update(@QueryParam("id") String id, @QueryParam("name") String name, @QueryParam("email") String email, @QueryParam("password") String passeord) {
        RegisterService.getInstance().update(new Register(Long.parseLong(id), name, email, passeord));
        return RegisterService.getInstance().findAll();
    }

    @Path("/delete")
    @GET
    @Produces("application/json")
    public String delete(@QueryParam("id") String id){
        RegisterService.getInstance().delete(new Register().setId(Long.parseLong(id)));
        return RegisterService.getInstance().findAll();
    }

    @Path("/findAll")
    @GET
    @Produces("application/json")
    public String findAll() {
        return RegisterService.getInstance().findAll();
    }
}
