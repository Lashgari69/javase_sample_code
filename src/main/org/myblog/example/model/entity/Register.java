package main.org.myblog.example.model.entity;

import java.io.Serializable;

public class Register implements Serializable {
    private Long id;
    private String name;
    private String email;
    private String password;

    public Register(){}

    public Register(String name, String email, String password){
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Register(long id, String name, String email, String password){
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public Register setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Register setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Register setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Register setPassword(String password) {
        this.password = password;
        return this;
    }
}
