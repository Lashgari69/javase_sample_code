package main.org.myblog.example.model.repository;

import main.org.myblog.example.common.JDBC;
import main.org.myblog.example.common.Logger;
import main.org.myblog.example.model.entity.Register;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RegisterDA implements AutoCloseable{

    private Connection connection;
    private PreparedStatement preparedStatement;

    public RegisterDA() {
        try {
            connection = JDBC.getConnection();
            connection.setAutoCommit(false);
        }catch (Exception e){
            Logger.error("There is an error on DB Connection !!! --> " + e.getMessage());
        }
    }

    public void insert(Register register) {
        try{
            preparedStatement = connection.prepareStatement("select register_seq.nextval nid from dual");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            register.setId(resultSet.getLong("nid"));
            preparedStatement = connection.prepareStatement("insert into register (id,name,email,password) values (?,?,?,?)");
            preparedStatement.setLong(1, register.getId());
            preparedStatement.setString(2, register.getName());
            preparedStatement.setString(3, register.getEmail());
            preparedStatement.setString(4, register.getPassword());
            preparedStatement.executeUpdate();
        }catch (Exception e){
            Logger.error("There is an error on register insert !!! --> " + e.getMessage());
        }
    }

    public void update(Register register) {
        try{
            preparedStatement = connection.prepareStatement("update register set name=?, email=?, password=? where id=?");
            preparedStatement.setString(1, register.getName());
            preparedStatement.setString(2, register.getEmail());
            preparedStatement.setString(3, register.getPassword());
            preparedStatement.setLong(4, register.getId());
            preparedStatement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
            Logger.error("There is an error on register update !!! --> " + e.getMessage());
        }
    }

    public void delete(Register register) {
        try{
            preparedStatement = connection.prepareStatement("delete from register where id=?");
            preparedStatement.setLong(1 , register.getId());
            preparedStatement.executeUpdate();
        }catch (Exception e){
            Logger.error("There is an error on register delete !!! --> " + e.getMessage());
        }
    }

    public String selectAll() throws Exception{
        preparedStatement = connection.prepareStatement("select * from register");
        ResultSet resultSet = preparedStatement.executeQuery();
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", resultSet.getString("id"));
            jsonObject.put("name", resultSet.getString("name"));
            jsonObject.put("email", resultSet.getString("email"));
            jsonObject.put("password", resultSet.getString("password"));
            jsonArray.add(jsonObject);
        }
        return jsonArray.toJSONString();
    }

    public void commit() throws Exception{
        connection.commit();
    }

    @Override
    public void close() throws Exception {
        preparedStatement.close();
        connection.close();
    }
}
