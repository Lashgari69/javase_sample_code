package main.org.myblog.example.model.service;

import main.org.myblog.example.common.Logger;
import main.org.myblog.example.model.entity.Register;
import main.org.myblog.example.model.repository.RegisterDA;

public class RegisterService {
    private static final RegisterService registerService = new RegisterService();

    private RegisterService(){}

    public static RegisterService getInstance() {
        return registerService;
    }

    public void save(Register register) {
        try(RegisterDA registerDA = new RegisterDA()){
            registerDA.insert(register);
            registerDA.commit();
        } catch (Exception e) {
            Logger.error(e.getMessage());
        }
    }

    public void update(Register register) {
        try(RegisterDA registerDA = new RegisterDA()){
            registerDA.update(register);
            registerDA.commit();
        }catch (Exception e){
            Logger.error(e.getMessage());
        }
    }

    public void delete(Register register) {
        try(RegisterDA registerDA = new RegisterDA()){
            registerDA.delete(register);
            registerDA.commit();
        }catch (Exception e){
            Logger.error(e.getMessage());
        }
    }

    public String findAll() {
        try(RegisterDA registerDA = new RegisterDA()){
            return registerDA.selectAll();
        }catch (Exception e){
            Logger.error(e.getMessage());
        }
        return null;
    }
}
