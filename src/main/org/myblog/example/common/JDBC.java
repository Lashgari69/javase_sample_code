package main.org.myblog.example.common;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;

public class JDBC {
    private static final BasicDataSource BASIC_DATA_SOURCE = new BasicDataSource();
    private static String userName = PropertiesCache.getInstance().getProperty("DB_USERNAME");
    private static String password = PropertiesCache.getInstance().getProperty("DB_PASSWORD");
    private static String port = PropertiesCache.getInstance().getProperty("PORT");
    private static String ipAddress = PropertiesCache.getInstance().getProperty("IP_ADDRESS");
    private static String dbName = PropertiesCache.getInstance().getProperty("DB_NAME");


    static {
        BASIC_DATA_SOURCE.setUsername(userName);
        BASIC_DATA_SOURCE.setPassword(password);
        BASIC_DATA_SOURCE.setUrl("jdbc:oracle:thin:@"+ipAddress+":"+port+":"+dbName+"");
        BASIC_DATA_SOURCE.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        BASIC_DATA_SOURCE.setMaxTotal(10);
    }

    public static Connection getConnection() throws Exception{
        return BASIC_DATA_SOURCE.getConnection();
    }
}
