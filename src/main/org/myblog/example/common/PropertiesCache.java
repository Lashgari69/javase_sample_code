package main.org.myblog.example.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class PropertiesCache {
    //singleton pattern
    private static class LazyHolder
    {
        private static final PropertiesCache INSTANCE = new PropertiesCache();
    }
    public static PropertiesCache getInstance()
    {
        return LazyHolder.INSTANCE;
    }

    private final Properties configProp = new Properties();

    private PropertiesCache()
    {
        InputStream  in = this.getClass().getClassLoader().getResourceAsStream("application.properties");
        //Private constructor to restrict new instances
        try {
            configProp.load(in);
            Logger.info("Reading all properties from the file");
        } catch (IOException e) {
            Logger.error(e.getMessage());
        }catch (Exception e){
            Logger.error(e.getMessage());
        }
    }



    public String getProperty(String key){
        return configProp.getProperty(key);
    }

    public Set<String> getAllPropertyNames(){
        return configProp.stringPropertyNames();
    }

    public boolean containsKey(String key){
        return configProp.containsKey(key);
    }
}
